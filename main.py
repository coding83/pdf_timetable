from datetime import time, date, datetime


def reader(filename):
    timetable_file = open(filename, "r")
    days = {"Пн": "mon", "Вт": "tue", "Ср": "wed", "Чт": "thu", "Пт": "fri", "Сб": "sat", "Вс": "sun"}
    array_of_tasks = []
    for line in timetable_file:
        line = line.strip(': \n')
        if line in days:
            day = days[line]
        else:
            array_of_line = line.split(", ")
            start_time = time.fromisoformat(array_of_line[0].zfill(5))
            end_time = time.fromisoformat(array_of_line[1].zfill(5))
            array_of_line[0] = start_time
            array_of_line[1] = end_time
            duration_time = datetime.combine(date.today(), end_time) - datetime.combine(date.today(), start_time)
            array_of_line.insert(0, day)
            array_of_line.insert(3, duration_time)
            array_of_tasks.append(array_of_line)
    return array_of_tasks
            

def tikzset(array_of_tasks):
    first_string = r'\tikzset{'
    tikzset_string = first_string + "\n"
    for i in range(len(array_of_tasks)):
        task_name = "task" + str(i)
        tikzset_string += task_name + "/.style={minimum height={t2f(" + str(array_of_tasks[i][3].seconds//3600) + ", " +  str(array_of_tasks[i][3].seconds//60%60) + r")*\yunit}}," + "\n"
    last_string = r'}'
    tikzset_string += last_string
    return tikzset_string

def tikz_coordinates(array_of_tasks):
    tikz_string = ""
    for i in range(len(array_of_tasks)):
        tikz_string += r'\coordinate (time' + str(i) + ') at (0,{t2f(' + str(array_of_tasks[i][1].hour) + ", " + str(array_of_tasks[i][1].minute) + ')});' + "\n"
    return tikz_string



def tikz_scope(array_of_tasks):
    first_string = r'\begin{scope}[every node/.style={draw,below right,fill=white,minimum width=\xunit,text width=\xunit,align=center,nosep}]' + "\n"
    tikz_string = first_string
    for i in range(len(array_of_tasks)):
        tikz_string += r'\node[task' + str(i) + r',fill=cyan!30] at (' + array_of_tasks[i][0] + r' |- time' + str(i) +r') {' + "\n" + r'\textbf{' + \
        array_of_tasks[i][-2] + r'}\\' + str(array_of_tasks[i][1].hour).zfill(2) + ":" + str(array_of_tasks[i][1].minute).zfill(2) + "-" + \
        str(array_of_tasks[i][2].hour).zfill(2) + ":" + str(array_of_tasks[i][2].minute).zfill(2) + r', ' + \
            array_of_tasks[i][-1] + "\n" + r'};' + "\n"
    last_string = r'\end{scope}' + "\n"
    tikz_string+=last_string
    return tikz_string


def gather_tikz_string(array_of_tasks):
    file = open("tikz_timetable.tex", "w")
    file.write(r"%%%%%%%%" + "\n")
    file.write(tikzset(array_of_tasks))
    file.write("\n")
    file.write(tikz_coordinates(array_of_tasks))
    file.write("\n")
    file.write(tikz_scope(array_of_tasks))
    file.write("\n")
    file.write(r"%%%%%%%%" + "\n")

if __name__ == '__main__':
    array_of_tasks = reader("timetable.txt")
    gather_tikz_string(array_of_tasks)
    
