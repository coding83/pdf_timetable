# Pdf Timetable Generator

Create beautiful pdf timetable in russian language for school, unviversity, etc. using latex and python.


## Description
Using this tool you can create pdf timetable with adjustable time for each lesson, meet or any activity. For creating timetable you just need to make simple timetable.txt. PDF file is in A4 format which is simple to print.

## Prerequisites
The script requires ```python3``` and ```datetime``` package, which may be installed via pip:


```pdflatex``` installation is required.

## Timetable txt file
The example of timetable.txt file is provided here. After name of the day you can write activity: 
start_time(HH:MM), end_time(HH:MM), activity_name? comment

## Get started
- edit `timetable.txt`
- run python script in terminal `python3 main.py`
- create pdf file from tex using terminal `pdflatex main.tex`


